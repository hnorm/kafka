package kafka

import (
	"fmt"
	"gitlab-ce.k8s.tools.vchangyi.com/scrm/backend/micro-scrm/internal/config"
	"testing"
)

func TestNewClient(t *testing.T) {
	var addrs = []string{"10.105.36.200:9092", "10.105.4.216:9092"}
	kaCdonfig := config.CfgData.Kafka
	var config = Config{
		Version: "V1_1_1_0",
	}
	config.Producer.Return.Successes = true
	client, err := NewClientWithPass(addrs, &config, &kaCdonfig)
	if err != nil {
		t.Errorf("newClient err:%v", err)
		return
	}
	producer, err := client.SyncProducer()
	if err != nil {
		t.Errorf("SyncProducer err:%v", err)
		return
	}
	index := 1
	for {
		msg := fmt.Sprintf("msg:%d", index)
		p, o, err := producer.SendMessage(&ProducerMessage{
			Topic: "scrm-test-demo-1",
			Key:   msg,
			Value: msg,
		})
		if err != nil {
			t.Errorf("SendMessage err:%v", err)
			break
		}
		fmt.Printf("sendMsg:%s, offset:%d, p:%d \n", msg, o, p)
		index++
	}

}
