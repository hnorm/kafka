package kafka

import (
	"fmt"
	"gitlab-ce.k8s.tools.vchangyi.com/common/go-toolbox/ctx"
	"gitlab-ce.k8s.tools.vchangyi.com/common/go-toolbox/helper"
	"os"
	"testing"
)

func Test_consumerGroup_Consume(t *testing.T) {
	var addrs = []string{"10.105.36.200:9092", "10.105.4.216:9092"}
	var config = Config{
		Version: "V1_1_1_0",
	}
	config.Producer.Return.Successes = true
	client, err := NewClient(addrs, &config)
	if err != nil {
		t.Errorf("newClient err:%v", err)
		return
	}
	consumerGroup, err := client.ConsumerGroup("group1")
	if err != nil {
		t.Errorf("ConsumerGroup err:%v", err)
		return
	}
	c := ctx.New()
	var handle ConsumerFunc = func(c ctx.Context, key string, value *string) error {
		fmt.Printf("consume1 msg, key:%s, value:%s \n", key, *value)
		return nil
	}
	consumerGroup.Consume(c, handle, []string{"scrm-test-demo-1"})

	client2, _ := NewClient(addrs, &config)
	consumerGroup2, err := client2.ConsumerGroup("group1")
	if err != nil {
		t.Errorf("ConsumerGroup err:%v", err)
		return
	}
	var handle2 ConsumerFunc = func(c ctx.Context, key string, value *string) error {
		fmt.Printf("consume2 msg, key:%s, value:%s \n", key, *value)
		return nil
	}
	consumerGroup2.Consume(c, handle2, []string{"scrm-test-demo-1"})

	helper.ObserveExitSignal(func(signal os.Signal) {
		consumerGroup.Close()
		fmt.Println("程序退出")
	})
}
