package kafka

import (
	"fmt"
	"gitlab-ce.k8s.tools.vchangyi.com/common/go-toolbox/ctx"
	"gitlab-ce.k8s.tools.vchangyi.com/common/go-toolbox/servers"
	"gitlab-ce.k8s.tools.vchangyi.com/scrm/backend/micro-scrm/internal/config"
	"testing"
)

func TestNewConsumerGroupServer(t *testing.T) {
	var serverGroup ServerGroup
	var handle ConsumerFunc = func(c ctx.Context, key string, value *string) error {
		fmt.Printf("consume1 msg, key:%s, value:%s \n", key, *value)
		return nil
	}
	var sconfig = Config{
		Version: "V1_1_1_0",
	}
	kafkaConfig := config.CfgData.Kafka
	sconfig.Producer.Return.Successes = true
	ServerInit([]string{"10.105.36.200:9092", "10.105.4.216:9092"}, &sconfig, kafkaConfig)
	serverGroup.Register(NewConsumerGroupServer("group", handle, []string{"scrm-test-demo-1"}, 2))
	groups := servers.Group(serverGroup.Servers()...)
	groups.ListenAndServe(func(context ctx.Context) {

	})
}
